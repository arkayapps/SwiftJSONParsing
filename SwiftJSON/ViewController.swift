//
//  ViewController.swift
//  SwiftJSON
//
//  Created by Suresh Kerai on 26/08/17.
//  Copyright © 2017 Suresh Kerai. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        print("test")

        let subjectID = 42;
        let servicesURL = Constants.MyWebServices.getChepterBySubject + "\(subjectID)";
        print(servicesURL)
        
        let url = URL(string: servicesURL);
        
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error != nil
            {
                print ("ERROR")
            }
            else
            {
                if let content = data
                {
                    do
                    {
                        //Array
                        let myJson = try JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                        
                        print(myJson)
                        for index in 0...myJson.count-1 {
                            let aObject = myJson[index] as! [String : AnyObject]
                            let chaptername = aObject["chaptername"] as! String
                            let chpid:Int? = Int(aObject["chapterid"] as! String)
                            let subid:Int? = Int(aObject["subjectid"] as! String)
                            
                            var chapter = Chapter(chaptername: chaptername, subjectid:  subid!, chapterid:chpid!)
                            print(chapter.chaptername)
                            print(chapter.chapterid)
                            print(chapter.subjectid)
                            
                            
                        }
                        
                        
                        
                    }
                    catch
                    {
                        
                    }
                }
            }
        }
        task.resume()
        
    
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

