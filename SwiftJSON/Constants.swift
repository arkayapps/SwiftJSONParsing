//
//  Constants.swift
//  SwiftJSON
//
//  Created by Suresh Kerai on 26/08/17.
//  Copyright © 2017 Suresh Kerai. All rights reserved.
//

import Foundation

struct Constants {
    
    struct MyWebServices {
        static let BaseUrl = "http://arkayapps.com/sellapp/edumcq/service/"
        static let subjectUrl = BaseUrl + "/getallsubject.php"
        static let getChepterBySubject = BaseUrl + "getchapterbysubject.php?subjectid="
    }
    
}
