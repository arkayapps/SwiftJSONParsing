//
//  Chapter.swift
//  SwiftJSON
//
//  Created by Suresh Kerai on 26/08/17.
//  Copyright © 2017 Suresh Kerai. All rights reserved.
//

import Foundation

class Chapter {
    var chaptername:String!
    var subjectid: Int!
    var chapterid: Int!
    
    init(chaptername: String, subjectid: Int, chapterid: Int ) { // Constructor
        self.chaptername = chaptername
        self.subjectid = subjectid
        self.chapterid = chapterid
    }
}
